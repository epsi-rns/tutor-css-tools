document.addEventListener('DOMContentLoaded', function(event) { 
  const tabHeaders  = document.getElementsByClassName('tab-headers')[0];
  const tabContents = document.getElementsByClassName('tab-contents')[0];
      
  Array.from(tabHeaders.children).forEach((targetHeader) => {
    // Tab Headers: All Click Events
    targetHeader.addEventListener('click', () => {
      const targetName = targetHeader.dataset.target;
      const colorClass = targetHeader.dataset.color;
      const targetContent = document.getElementById(targetName);

      // Set all to default setting
      Array.from(tabHeaders.children).forEach((tabHeader) => {
        tabHeader.classList.remove('active');
        tabHeader.classList.remove(tabHeader.dataset.color);
        tabHeader.classList.add('bg-gray-700');
      });
      // Except the chosen one
      targetHeader.classList.add('active');
      targetHeader.classList.remove('bg-gray-700');
      targetHeader.classList.add(colorClass);

      // Showing the content
      Array.from(tabContents.children).forEach((tabContent) => {
        tabContent.style.display = 'none';
      });
      targetContent.style.display = 'block';
      targetContent.classList.add(colorClass);
    });
  });

  // Tab Headers: Default
  tabHeaders.getElementsByClassName('active')[0].click();
});
