import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import AlohaWorld from '@/components/AlohaWorld'
import YelloWorld from '@/components/YelloWorld'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'default',
      component: YelloWorld,
      meta: { title: 'Yellow' }
    },
    {
      path: '/hello',
      name: 'Hello',
      component: HelloWorld,
      meta: { title: 'Hello' }
    },
    {
      path: '/aloha',
      name: 'Aloha',
      component: AlohaWorld,
      meta: { title: 'Aloha' }
    },
    {
      path: '/yellow',
      name: 'Yellow',
      component: YelloWorld,
      meta: { title: 'Yellow' }
    }
  ]
})
