module.exports = function(grunt) {
  // configure the tasks
  let config = {

    // Pug
    pug: {
      compile: {
        options: {
          data: {
            debug: false
          },
          pretty: true,
          filters: {
            'stylus': function(block) {
              return require('stylus').render(block);
            },
            'coffee-script': function(block) {
              return require('coffeescript').compile(block);
            },
          },
        },
        files: [ {
          cwd: "pug",
          src: "*.pug",
          dest: "html",
          expand: true,
          ext: ".html"
        } ]
      }
    },

    // Stylus
    stylus: {
      compile: {
        options: {
          compress: false
        },
        files: {
          'css/style.css': 'css/style.styl' // 1:1 compile
        }
      }
    },

    // Coffeescript
    coffee: {
      compile: {
        files: {
          'js/script.js': 'js/script.coffee' // 1:1 compile
        }
      }
    },

    //  Watch Files
    watch: {
      pug: {
        files: ['pug/**/*'],
        tasks: ['pug'],
        options: {
          interrupt: false,
          spawn: false
        }
      },
      stylus: {
        files: ['css/style.styl'],
        tasks: ['stylus']
      },
      coffee: {
        files: ['js/script.coffee'],
        tasks: ['coffee']
      }
    }

  };

  grunt.initConfig(config);

  // load the tasks
  grunt.loadNpmTasks('grunt-contrib-pug');
  grunt.loadNpmTasks('grunt-contrib-stylus');
  grunt.loadNpmTasks('grunt-contrib-coffee');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // define the tasks
  grunt.registerTask('default', [
    'pug', 'stylus', 'coffee', 'watch'
  ] );

};
