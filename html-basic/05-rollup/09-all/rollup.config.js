import serve from 'rollup-plugin-serve';
import livereload from 'rollup-plugin-livereload'

import html from 'rollup-plugin-generate-html-template';
import css from 'rollup-plugin-css-only';

import stylus from 'rollup-plugin-stylus-compiler';
import coffeescript from 'rollup-plugin-coffee-script';

export default {
  input: 'src/entry.js',
  output: { 
    file: 'build/js/script.js',
    format: 'iife'
  },
  plugins: [
    stylus(),
    coffeescript(),
    css({ 
      output: 'build/css/style.css' 
    }),
    html({
      template: 'src/html/alert.html',
      target: 'build/index.html',
      inject: 'head',
      externals: [
        { type: 'js', file: "js/script.js", pos: 'before' }
      ]
    }),
    serve('build'),
    livereload({
      watch: ['dist', 'src']
    })
  ]
}
