#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'epsi'
# SITENAME = 'Yet Another Example'
SITEURL = ''

THEME = "tutor"

PATH = 'content'

TIMEZONE = 'Asia/Jakarta'

DEFAULT_LANG = 'en'

DEFAULT_PAGINATION = 10

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

INDEX_URL             = 'blog/'
INDEX_SAVE_AS         = 'blog/index.html'

# Data
PAGES = (
  { "link": "/",                "short": "Home", "long": "Home"  },
  { "link": "/pages/html.html", "short": "HTML", "long": "HTML Link" },
  { "link": "/pages/css.html",  "short": "CSS",  "long": "CSS Link" },
  { "link": "/pages/php.html",  "short": "PHP",  "long": "PHP Link" },
  { "link": "/pages/javascript.html", "short": "Javascript", "long": "Javascript Link" }
)
