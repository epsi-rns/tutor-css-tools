import css from 'rollup-plugin-css-only';

export default {
  input: 'src/entry.js',
  output: { 
    file: 'build/js/script.js',
    format: 'iife'
  },
  plugins: [
    css({ 
      output: 'build/css/style.css' 
    })
  ]
}
