document.addEventListener("DOMContentLoaded", function(event) { 
  var alertButtons = document.getElementsByClassName("dismissable");

  for (var i=0; i < alertButtons.length; i++) {
    alertButtons[i].onclick = function() {
      this.parentElement.style.display='none';
      console.log('Close Button. Element ' + this.parentElement.id + ' dismissed');
      return false;
    };
  };
});
