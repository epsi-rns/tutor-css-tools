$(document).ready(function() {
  //const tabContents = $('.tab-contents').first();

  // Tab Headers: All Click Events
  $('.tab-headers > div').on('click', function (event) {
    const targetName = $(this)[0].dataset.target;
    const colorClass = $(this)[0].dataset.color;
    
    // Set all to default setting
    $('.tab-headers > div').each(function(el, i) {
      $(this)
        .removeClass('active')
        .removeClass(el.dataset.color)
        .addClass('bg-gray-700')
      $(el.firstElementChild).removeClass('bg-white');
    });
    // Except the chosen one
    $(this)
      .addClass('active')
      .removeClass('bg-gray-700')
      .addClass(colorClass)
    $($(this)[0].firstElementChild).addClass('bg-white');

    // Showing the content
    $('.tab-contents > div').each(function(el, i) {
      el.style.display = 'none';
    });
    sel = $('#'+targetName);
    sel[0].style.display = 'block';
    sel.addClass(colorClass);
  });

  // Tab Headers: All Click Events
  $('.tab-headers > div').on('mouseenter', function (event) {
    const targetName = $(this)[0].dataset.target;
    $($('#'+targetName)[0].firstElementChild).addClass('is-hovered');
  });

  // Tab Headers: All Click Events
  $('.tab-headers > div').on('mouseleave', function (event) {
    const targetName = $(this)[0].dataset.target;
    $($('#'+targetName)[0].firstElementChild).removeClass('is-hovered');
  });

  // Tab Headers: Trigger Default
  const tabHeaders  = $('.tab-headers').first();
  $('.active', tabHeaders)[0].click();
});
