
(function(l, r) { if (l.getElementById('livereloadscript')) return; r = l.createElement('script'); r.async = 1; r.src = '//' + (window.location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1'; r.id = 'livereloadscript'; l.getElementsByTagName('head')[0].appendChild(r) })(window.document);
(function () {
  'use strict';

  document.addEventListener("DOMContentLoaded", function(event) { 
    var alertButtons = document.getElementsByClassName("dismissable");
      
    for (var i=0; i < alertButtons.length; i++) {
      alertButtons[i].onclick = function() {
        this.parentElement.style.display='none';
        console.log('Close Button. Element ' + this.parentElement.id + ' dismissed');
        return false;
      };
    }});

}());
