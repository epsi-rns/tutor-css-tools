// https://stackoverflow.com/questions/36376645/how-to-use-autoprefixer-with-postcss-and-grunt-on-more-than-one-file#36442427

module.exports = function(grunt) {
  // configure the tasks
  let config = {

    postcss: {
      options: {
        map: true, // inline sourcemaps
        syntax: require('postcss-scss'),
        processors: [
          require('postcss-strip-inline-comments'),
          require('postcss-each'),
          require('precss'),
          require('postcss-prettify'),
        ]
      },
      dist: {
        files: [
          {
            src: 'dest/*.css'
          }
        ]
      }
    },

    copy: {
      postcss: {
        files: [
          {
            src: 'src/*.css',
            dest: './dest/',
            expand: true,
            flatten: true
          }
        ]
      }
    },

    watch: {
      postcss: {
        files: ['src/*'],
        tasks: ['postcss'],
        options: {
          interrupt: false,
          spawn: false
        }
      },
    }

  };

  grunt.initConfig(config);

  // load the tasks
  grunt.loadNpmTasks('grunt-postcss');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('css', ['copy:postcss', 'postcss']);

  // define the tasks
  grunt.registerTask('default', [
    'css', 'watch'
  ] );

};
