import { createWebHistory, createRouter } from "vue-router";
import MainMockup from   '@/components/mockup/MainMockup'
import MainSimple from   '@/components/simple/MainSimple'
import MainEnhanced from '@/components/enhanced/MainEnhanced'

const routes = [
  {
    path: '/',
    name: 'default',
    component: MainEnhanced,
    meta: { title: 'Default Page' }
  },
  {
    path: '/simple-mockup',
    name: 'MainMockup',
    component: MainMockup,
    meta: { title: 'Simple Tabs - Mockup' }
  },
  {
    path: '/simple-tabs',
    name: 'MainSimple',
    component: MainSimple,
    meta: { title: 'Simple Tabs - Component' }
  },
  {
    path: '/enhanced-tabs',
    name: 'MainEnhanced',
    component: MainEnhanced,
    meta: { title: 'Enhanced Tabs - Component' }
  }
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
