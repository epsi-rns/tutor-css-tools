const Koa      = require('koa');
const Router   = require('koa-router');
const nunjucks = require('koa-nunjucks-2');
const path     = require('path');
const serve    = require('koa-static');

const app = new Koa();
const router = new Router();

// Example Pages
const pages = [
  { link: '/',     short: 'Home', long: 'Home'  },
  { link: '/html', short: 'HTML', long: 'HTML Link' },
  { link: '/css',  short: 'CSS',  long: 'CSS Link' },
  { link: '/php',  short: 'PHP',  long: 'PHP Link' },
  { link: '/javascript', short: 'Javascript', long: 'Javascript Link' }
];

// Static Assets
app.use(serve('public'));

// Render Nunjucks
app.use(nunjucks({
  ext: 'njk',
  path: path.join(__dirname, 'views'),
  nunjucksConfig: {
    trimBlocks: true
  }
}));

// Router
router.get('/', async ctx => {
  console.log('Showing index');
  await ctx.render('index', { pages: pages });
});

router.get('/:page', async ctx => {
  const choices = ['index', 'html', 'css', 'php', 'javascript'];
  const page = ctx.params.page;
  
  if (choices.includes(page)) {
    console.log('Showing ' + page);
    await ctx.render(page, { pages: pages });
  } else {
    console.log('404: Page not Found: '+ page);
    ctx.throw(404, '404: Page not Found');
  }
});

app.use(router.routes());

// Run, Baby Run
app.listen(3000);
console.log('Koa started on port 3000');


