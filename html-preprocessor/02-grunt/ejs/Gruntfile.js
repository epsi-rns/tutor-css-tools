module.exports = function(grunt) {
  // configure the tasks
  let config = {

    ejs: {
      all: {
        cwd: 'views',
        src: ['**/*.ejs', '!partials/**/*'],
        dest: 'build/',
        expand: true,
        ext: '.html'
      }
    },

    watch: {
      ejs: {
        files: ['views/**/*'],
        tasks: ['ejs'],
        options: {
          livereload: true,
          interrupt: false,
          spawn: false
        }
      }
    }

  };

  grunt.initConfig(config);

  // load the tasks
  grunt.loadNpmTasks('grunt-ejs');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // define the tasks
  grunt.registerTask('default', [
    'ejs', 'watch'
  ] );

};
