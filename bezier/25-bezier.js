document.addEventListener(
  "DOMContentLoaded", function(event) {
    const svg_root  = document.getElementById("svg_root");

    const xmlns = 'http://www.w3.org/2000/svg';
    const blueScale = [     '#E3F2FD', 
      '#BBDEFB', '#90CAF9', '#64B5F6',
      '#42A5F5', '#2196F3', '#1E88E5',
      '#1976D2', '#1565C0', '#0D47A1'];

    // Initialize base points
    // with destructuring style
    let [mx, my] = [0, 190];
    let [c1x, c2x, c3x] = [-10, 70, 120];
    let c1y, c2y, c3y;
    let [s1x, s1y, s2x, s2y] = [170, 230, 200, 220];

    [...Array(10)].forEach((_, index) => {

      // Defining variant points
      my  -= 20;
      c1x += 10;
      [c1y, c2y, c3y] = [my-10, my-10, my+10]
      s1y -= 20;
      s2y -= 20;

      const d_property = 
              `M ${mx}  ${my}
               C ${c1x} ${c1y}, ${c2x} ${c2y}, ${c3x} ${c3y}
               S ${s1x} ${s1y}, ${s2x} ${s2y}
               L   200,     0  L    0,     0
               Z ${mx}  ${my}`;

      // material blue index 0i
      const area = document.createElementNS(xmlns,"path"); 
      area.setAttribute("class", "area");
      area.setAttribute("fill", blueScale[index]);
      area.setAttribute("d", d_property);

      svg_root.appendChild(area);
    });
});
